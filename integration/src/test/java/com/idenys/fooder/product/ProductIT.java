package com.idenys.fooder.product;

import com.idenys.fooder.model.Product;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class ProductIT {

  @Test
  void createProduct() {

    Product product = new Product();
    product.setName("morkovka");
    product.setDescription("the very best ovosh");

    Product as = given()
            .baseUri("http://localhost:8080/api/v1/products")
            .contentType(ContentType.JSON)
            .log()
            .all()
            .when()
            .body(product)
            .post()
            .then()
            .assertThat()
            .statusCode(200)
            .log()
            .all().extract().body().as(Product.class);
  }
}
